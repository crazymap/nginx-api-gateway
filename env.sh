#停止、禁用 firewalld
systemctl stop firewalld
systemctl disable firewalld

#禁用selinux
sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config
setenforce 0

#配置ssh
sed -i 's/^GSSAPIAuthentication yes$/GSSAPIAuthentication no/' /etc/ssh/sshd_config
sed -i 's/#UseDNS yes/UseDNS no/' /etc/ssh/sshd_config
systemctl restart sshd.service
systemctl enable sshd.service

#清理默认YUM源
rm -rf /etc/yum.repos.d/*

#修改DNS地址
cat > /etc/resolv.conf << EOF
nameserver 223.5.5.5
nameserver 223.6.6.6
EOF

#更新为阿里云YUM源
curl -o /etc/yum.repos.d/Centos-7.repo http://mirrors.aliyun.com/repo/Centos-7.repo

#安装 基本软件
yum -y install unzip wget ntp net-tools

#更新系统中的YUM包
yum update -y

# 更新时间 配置每天凌晨4点更新时间
timedatectl set-timezone "Asia/Shanghai"
/usr/sbin/ntpdate cn.pool.ntp.org
echo "* 4 * * * /usr/sbin/ntpdate cn.pool.ntp.org > /dev/null 2>&1" >> /var/spool/cron/root
systemctl restart crond.service
systemctl enable crond

# 配置VI编辑器
cat >> /etc/virc << EOF
set tabstop=4
set shiftwidth=4
set expandtab
syntax on
set nu
EOF

# 设置主机名称
/usr/bin/hostnamectl set-hostname wuweiit

# 内核调优
# 端口最大的监听队列的长度
#net.core.somaxconn = 2048
#sysctl -p
# 开启 tcp_tw_recycle
#echo 1 > /proc/sys/net/ipv4/tcp_tw_recycle
#开启tcp_tw_reuse
#echo 1 > /proc/sys/net/ipv4/tcp_tw_reuse
#echo 0 > /proc/sys/net/ipv4/tcp_syncookies
