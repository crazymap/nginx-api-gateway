#编译Nginx 依赖包安装
/usr/bin/yum install gcc gcc-c++ automake pcre pcre-devel zlip zlib-devel openssl openssl-devel -y

#停止现有NGINX
/usr/bin/systemctl stop nginx

rm -rf /data/backup/nginx-1.16.1.tar.gz
rm -rf /data/backup/nginx-modules/*
rm -rf /usr/lib64/nginx/modules/*
mkdir -p /data/backup/nginx-modules/
/usr/bin/wget http://nginx.org/download/nginx-1.16.1.tar.gz -O /data/backup/nginx-1.16.1.tar.gz
/usr/bin/tar -zxf /data/backup/nginx-1.16.1.tar.gz -C /data/backup

#Nginx 模块下载
wget http://openresty.org/download/drizzle7-2011.07.21.tar.gz  -O /data/backup/nginx-modules/drizzle7-2011.07.21.tar.gz
wget https://github.com/openresty/drizzle-nginx-module/archive/master.zip -O /data/backup/nginx-modules/drizzle-nginx-module.zip
wget https://github.com/openresty/rds-json-nginx-module/archive/master.zip -O /data/backup/nginx-modules/rds-json-nginx-module.zip
wget https://github.com/simplresty/ngx_devel_kit/archive/master.zip -O /data/backup/nginx-modules/ngx_devel_kit.zip
wget https://github.com/openresty/set-misc-nginx-module/archive/master.zip -O /data/backup/nginx-modules/set-misc-nginx-module.zip
wget https://github.com/openresty/echo-nginx-module/archive/master.zip -O /data/backup/nginx-modules/echo-nginx-module.zip
wget https://github.com/openresty/nginx-eval-module/archive/master.zip -O /data/backup/nginx-modules/nginx-eval-module.zip
wget https://github.com/calio/form-input-nginx-module/archive/master.zip -O /data/backup/nginx-modules/form-input-nginx-module.zip

#Nginx模块目录指定
/usr/bin/unzip /data/backup/nginx-modules/drizzle-nginx-module.zip -d /usr/lib64/nginx/modules/
/usr/bin/unzip /data/backup/nginx-modules/rds-json-nginx-module.zip -d /usr/lib64/nginx/modules/
/usr/bin/unzip /data/backup/nginx-modules/ngx_devel_kit.zip -d /usr/lib64/nginx/modules/
/usr/bin/unzip /data/backup/nginx-modules/set-misc-nginx-module.zip -d /usr/lib64/nginx/modules/
/usr/bin/unzip /data/backup/nginx-modules/echo-nginx-module.zip -d /usr/lib64/nginx/modules/
/usr/bin/unzip /data/backup/nginx-modules/nginx-eval-module.zip -d /usr/lib64/nginx/modules/
/usr/bin/unzip /data/backup/nginx-modules/form-input-nginx-module.zip -d /usr/lib64/nginx/modules/

/usr/bin/mv /usr/lib64/nginx/modules/drizzle-nginx-module-master /usr/lib64/nginx/modules/drizzle-nginx-module
/usr/bin/mv /usr/lib64/nginx/modules/ngx_devel_kit-master /usr/lib64/nginx/modules/ngx_devel_kit
/usr/bin/mv /usr/lib64/nginx/modules/rds-json-nginx-module-master /usr/lib64/nginx/modules/rds-json-nginx-module
/usr/bin/mv /usr/lib64/nginx/modules/set-misc-nginx-module-master /usr/lib64/nginx/modules/set-misc-nginx-module
/usr/bin/mv /usr/lib64/nginx/modules/echo-nginx-module-master /usr/lib64/nginx/modules/echo-nginx-module
/usr/bin/mv /usr/lib64/nginx/modules/nginx-eval-module-master /usr/lib64/nginx/modules/nginx-eval-module
/usr/bin/mv /usr/lib64/nginx/modules/form-input-nginx-module-master /usr/lib64/nginx/modules/form-input-nginx-module

#drizzle7 安装
/usr/bin/tar -zxf /data/backup/nginx-modules/drizzle7-2011.07.21.tar.gz -C /data/backup/nginx-modules
cd /data/backup/nginx-modules/drizzle7-2011.07.21
./configure --without-server 
make libdrizzle-1.0 
make install-libdrizzle-1.0
ln -s /usr/local/lib/libdrizzle.so.1 /lib64/libdrizzle.so.1

#Nginx 编译新增加模块
cd /data/backup/nginx-1.16.1/

./configure --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib64/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic -fPIC' --with-ld-opt='-Wl,-z,relro -Wl,-z,now -pie' --add-module=/usr/lib64/nginx/modules/drizzle-nginx-module --add-module=/usr/lib64/nginx/modules/ngx_devel_kit --add-module=/usr/lib64/nginx/modules/rds-json-nginx-module --add-module=/usr/lib64/nginx/modules/set-misc-nginx-module --add-module=/usr/lib64/nginx/modules/echo-nginx-module --add-module=/usr/lib64/nginx/modules/nginx-eval-module  --add-module=/usr/lib64/nginx/modules/form-input-nginx-module

#编译
make
cp /usr/sbin/nginx /usr/sbin/nginx-backup
cp /data/backup/nginx-1.16.1/objs/nginx /usr/sbin/nginx

